using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_zadataci
{
    class Die
    {
        private int numberOfSides;
        private RandomGenerator randomGenerator;
        //private Random randomGenerator;//1.zad i 2.zad

        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            //this.randomGenerator = new Random();//1.zad i 2.zad
            this.randomGenerator = RandomGenerator.GetInstance();//3.zad 
        }
        public int Roll()
        {
            return randomGenerator.NextInt(1, numberOfSides + 1);//3.zad
            //return randomGenerator.Next(1, numberOfSides + 1);//1.zad i 2.zad
        }
        //7.zad
        public int GetNumberOfSides()
        {
            return this.numberOfSides;
        }
        //7.zad end
    }
}
