using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_zadataci
{
    class FlexibleDiceRoller: IFlexibleDiceRoller, ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        public FlexibleDiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }
        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        //7.zad
        public void RemoveDice(int NumberOfSides)
        {
            this.dice.RemoveAll(dice=>dice.GetNumberOfSides() == NumberOfSides);
        }
        public string GetStringRepresentation()
        {
            List<int> Out=new List<int>();
            for(int i = 0; i < dice.Count; i++)
            {
                Out.Add(dice[i].GetNumberOfSides());
            }
            return string.Join(" ", Out);
        }
        //7.zad end

    }
}
