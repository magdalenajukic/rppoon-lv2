using System.Text;

namespace LV2_zadataci
{
    interface ILogable
    {
        string GetStringRepresentation();
    }
}
