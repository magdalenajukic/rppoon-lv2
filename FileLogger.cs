using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace LV2_zadataci
{
    class FileLogger:ILogger
    {
        private StreamWriter file;
        public FileLogger(string path)
        {
            file = new StreamWriter(path);
        }
        public void Log(string message)
        {
            file.WriteLine(message);
        }
        public void Close()
        {
            file.Close();
        }

        //5.zad
        public void Log(ILogable data)
        {
            file.WriteLine(data.GetStringRepresentation());
        }
        //5.zad end
    }
}
