using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_zadataci
{
    class ConsoleLogger:ILogger
    {
        private string console;
        public ConsoleLogger() { }
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
        //5.zad
        public void Log(ILogable data)
        {
            Console.WriteLine(data.GetStringRepresentation());
        }
        //5.zad end
    }
}