using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_zadataci
{
    interface IFlexibleDiceRoller:IDiceRoller
    {
        void InsertDie(Die die);
        void RemoveAllDice();
    }
}
