using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_zadataci
{
    interface IDiceRoller
    {
        
        void RemoveAllDice();
        void RollAllDice();

    }
}