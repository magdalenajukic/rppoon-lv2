using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_zadataci
{
    interface ILogger
    {
        void Log(string message);
        void Log(ILogable data);
    }
}