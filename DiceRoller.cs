using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_zadataci
{
    class DiceRoller:ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        //4.zad
        private ILogger log;
                           
        //public DiceRoller()
        //{
        //    this.dice = new List<Die>();
        //    this.resultForEachRoll = new List<int>();
        //}
        public DiceRoller(ILogger logger)
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            this.log = logger;
        }
        //4.zad end

        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RollAllDice()
        {
            //clear results of previous rolling
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        //View of the results
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
                this.resultForEachRoll
           );
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }
        //5.zad
        public string GetStringRepresentation()
        {
            return string.Join(" ", GetRollingResults());

        }
        //5.zad end

    }
}